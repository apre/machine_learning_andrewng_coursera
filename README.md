# Machine Learning from scratch with numpy

This project is an effort to implement in python all lessons in Andrew Ng's 
Machine Learning course offered by Coursera.

This project is mostly done using numpy, but other SciPy packages were used,
like matplotlib and the SciPy fundamental library.