import numpy as np


def compute_cost(X, y, theta):
    """
    Compute cost for linear regression with multiple variables.

    :param X: dataset points. Shape is (num_examples, num_features)
    :param y: dataset values. Shape is (num_examples, 1)
    :param theta: function parameters. Shape is (num_features,)
    :return: the cost of using theta as the parameter for linear regression
        to fit the data points in X and y
    """
    return np.mean(np.square(np.dot(X, theta) - y.ravel())) / 2
