import numpy as np


def normal_eqn(X, y):
    """
    Computes the closed-form solution to linear regression
    using the normal equations.

    :param X: dataset points. Shape is (num_examples, num_features)
    :param y: dataset values. Shape is (num_examples, 1)
    :return theta: closed form solution to the linear regression parameters
    """
    pseudo_inverse = np.linalg.pinv(np.dot(X.T, X))
    return pseudo_inverse @ X.T @ y
