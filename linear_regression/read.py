import numpy as np


def line_to_array(line):
    return [float(el) for el in line.split(',')]


def read_file(filename):
    with open(filename) as f:
        lines = f.readlines()

    return np.array(list(map(line_to_array, lines)))


if __name__ == '__main__':
    arr = read_file('ex1data1.txt')
    print(arr)
