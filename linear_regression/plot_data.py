import matplotlib.pyplot as plt


def plot_data(x, y):
    """
    Plots the data points and gives the figure axes labels of
    population and profit

    :param x: population data.
    :param y: profit data.
    """

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x, y, marker='+', c='r')
    plt.show()


if __name__ == '__main__':
    plot_data([1, 2, 3], [2, 3, 4])
