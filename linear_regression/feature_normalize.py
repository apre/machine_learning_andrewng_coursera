import numpy as np


def feature_normalize(X):
    """
    Returns a normalized version of X where the mean value
    of each feature is 0 and the standard deviation is 1.

    :param X: dataset points to be normalized
    :return: (x_norm, mu, std): normalized dataset points, the
        original mean and original standard deviation.
    """
    # X is (num_examples, num_features)
    mu = np.mean(X, axis=0)
    std = np.std(X, axis=0)

    x_norm = (X - mu) / std

    return x_norm, mu, std

