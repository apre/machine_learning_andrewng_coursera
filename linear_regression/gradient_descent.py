import numpy as np

from cost_function import compute_cost


def gradient_descent(X, y, theta, alpha, num_iters):
    """
    Performs gradient descent to learn theta

    :param X: dataset points. Shape is (num_examples, num_features)
    :param y: dataset values. Shape is (num_examples, 1)
    :param theta: initial value for parameters vector. Shape is (num_features, )
    :param alpha: learning rate
    :param num_iters: number of learning iterations
    :return: (theta, j_history): The learned theta and the history of cost values
    """

    j_history = []
    for i in range(num_iters):
        j = compute_cost(X, y, theta)
        j_history.append(j)

        grad = np.dot((np.dot(X, theta) - y.ravel()), X)
        theta = theta - alpha * grad

    return theta, j_history


if __name__ == '__main__':
    num_examples = 10
    num_features = 5

    X = np.ones([num_examples, num_features])
    y = np.ones([num_examples, 1])
    theta = np.random.normal(0, 1, num_features)
    alpha = 1e-3
    num_iters = 10
    theta, j_history = gradient_descent(X, y, theta, alpha, num_iters)
    print(j_history)
