import numpy as np
import matplotlib.pyplot as plt

from cost_function import compute_cost
from feature_normalize import feature_normalize
from gradient_descent import gradient_descent
from normal_eqn import normal_eqn
from plot_data import plot_data
from read import read_file

# Part 1: plotting
print('Plotting Data...')
data = read_file('ex1data2.txt')
X = data[:, 0:2]
y = data[:, 2]
m = len(y)

# Print out some data points
print('First 10 examples from the dataset: ')
print(f'x = {X[0:10, :]}, \ny = {y[0:10]}')

# Scale features and set them to zero mean
print('Normalizing features... ')
X, mu, sigma = feature_normalize(X)

# Add intercept term to X
X = np.hstack([np.ones([m, 1]), X])

# Part 2: Gradient Descent
print('Running gradient descent.... ')

# Choose some alpha value
alpha = .01
num_iters = 400

# Init theta and run gradient descent
theta = np.random.normal(loc=0, scale=1, size=3)
theta, j_history = gradient_descent(X, y, theta, alpha, num_iters)

# Plot the convergence graph
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(j_history)
ax.set_xlabel('Number of iterations')
ax.set_ylabel('Cost J')

# Display gradient descent's result
print('Theta computed from gradient descent: ')
print(theta)
print('')

# Estimate the price of a 1650 sq-ft, 3 br house
x = np.array([1650, 3])
x = (x - mu)/sigma
x = np.concatenate([[1], x])
price = np.dot(x, theta)
print(f'Predicted price of a 1650 sq-ft, 3 br house using'
      f'gradient descent: {price}')


# Part 3: Normal Equations
print('Solving with normal equations... ')

# Load data
data = read_file('ex1data2.txt')
X = data[:, 0:2]
y = data[:, 2]
m = len(y)
X = np.hstack([np.ones([m, 1]), X])

# Calculate the parameters from the normal equation
theta = normal_eqn(X, y)

# Display equation's result
print('Theta computed from the normal equations: ')
print(theta)
print('')

# Estimate the price of a 1650 sq-ft, 3 br house
x = np.array([1, 1650, 3])
price = np.dot(x, theta)
print(f'Predicted price of a 1650 sq-ft, 3 br house using'
      f'gradient descent: {price}')