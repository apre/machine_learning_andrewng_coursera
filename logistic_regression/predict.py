import numpy as np

from sigmoid import sigmoid


def predict(theta, X):
    """
    Predict whether the label is 0 or 1 using learned logistic regression parameters theta

    :param theta: Learned logistic regression parameters. Shape is (num_features,)
    :param X: points to be evaluated. Shape is (num_points, num_features)
    :return: prediction, 0 or 1
    """
    return np.round(sigmoid(np.dot(X, theta)))
