import numpy as np


def map_feature(x1, x2):
    """
    Feature mapping function to polynomial features.
    Inputs x1 and x1 must be the same size.
    First column is always x1^0 * x2^0 = 1

    :param x1: first input feature
    :param x2: second input feature
    :return x: feature array with quadratic features. Shape is (num_examples, num_features)
    """
    degree = 3
    x = np.ones([x1.shape[0], int((degree + 2) * (degree + 1)/2)])

    k = 0
    for i in range(degree + 1):
        for j in range(i + 1):
            x[:, k] = np.power(x1, i-j) * np.power(x2, j)
            k += 1

    return x


if __name__ == '__main__':
    x1 = np.array([1, 3, 5, 7])
    x2 = np.array([1, 10, 5, 9])

    print(map_feature(x1, x2))
