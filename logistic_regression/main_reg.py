import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fmin

from read import read_file
from plot_data import plot_data, plot_decision_boundary
from cost import cost_function, cost_function_reg
from sigmoid import sigmoid
from predict import predict
from map_feature import map_feature

# Load data
data = read_file('ex2data2.txt')
X = data[:, 0:2]
y = data[:, 2]

# Plot data
fig = plt.figure()
ax = fig.add_subplot(111)
plot_data(ax, X, y, labels=['y = 1', 'y = 0'])
plt.xlabel('Microchip Test 1')
plt.ylabel('Microchip Test 2')
plt.legend()
# plt.show()

# Part 1: Regularized Logistic Regression
# Add Polynomial Features and the column of ones
X = map_feature(X[:, 0], X[:, 1])

# Initialize fitting parameters
initial_theta = np.zeros(X.shape[1])

# Set regularization parameter lambda to 1
lamb = 1

# Compute and display initial cost and gradient
[cost, grad] = cost_function_reg(initial_theta, X, y, lamb)

print(f'Cost at initial theta (zeros): {cost}\n'
      f'Expected cost (approx): 0.693\n'
      f'Gradient at initial theta (zeros) - first five values only:\n'
      f'{grad[:5]}\n'
      f'Expected gradients (approx) - first five values only:\n '
      f' 0.0085\n 0.0188\n 0.0001\n 0.0503\n 0.0115\n')

# Compute and display cost and gradient with all-ones theta and lambda
test_theta = np.ones(X.shape[1])
[cost, grad] = cost_function_reg(test_theta, X, y, 10)

print(f'Cost at test theta (with lambda = 10): {cost}\n'
      f'Expected cost (approx): 3.16\n'
      f'Gradient at test theta - first five values only:\n'
      f'{grad}\n'
      f'Expected gradients (approx) - first five values only:\n'
      f' 0.3460\n 0.1614\n 0.1948\n 0.2269\n 0.0922\n')

# Part 2: Regularization and Accuracies
# Initialize fitting parameters
initial_theta = np.zeros(X.shape[1])

# Set regularization parameter to the one you want
lamb = 1

# Optimize
theta = fmin(lambda x: cost_function(x, X, y)[0], initial_theta, disp=False)

# Plot boundary
fig = plt.figure()
ax = fig.add_subplot(111)
plot_decision_boundary(ax, theta, X, y, labels=['y = 1', 'y = 0'])
ax.set_xlabel('Microchip Test 1')
ax.set_ylabel('Microchip Test 2')

# Compute accuracy on outr training set
p = predict(theta, X)
print(f'Train accuracy: {np.mean(p == y) * 100}%')
print(f'Expected accuracy = 89.0%')
plt.show()
