import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fmin

from read import read_file
from plot_data import plot_data, plot_decision_boundary
from cost import cost_function
from sigmoid import sigmoid
from predict import predict

# Load data
data = read_file('ex2data1.txt')
X = data[:, 0:2]
y = data[:, 2]

# Part 1: Plotting
fig = plt.figure()
ax = fig.add_subplot(111)
plot_data(ax, X, y, labels=['Admitted', 'Not admitted'])
plt.xlabel('Exam 1 score')
plt.ylabel('Exam 2 score')
plt.legend()
# plt.show()

# Part 2: Compute Cost and Gradient
# Add intercept term to x and X_test
num_examples = len(X)
X = np.hstack([np.ones([num_examples, 1]), X])

# Initialize fitting parameters
initial_theta = np.zeros(X.shape[1])

# Compute and display initial cost and gradient
[cost, grad] = cost_function(initial_theta, X, y)

print(f'Cost at initial theta (zeros): {cost}\n'
      f'Expected cost (approx): 0.693\n'
      f'Gradient at initial theta (zeros):\n'
      f'{grad}\n'
      f'Expected gradients (approx):\n -0.1000\n -12.0092\n -11.2628\n')

# Compute and display cost and gradient with non-zero theta
test_theta = [-24, .2, .2]
[cost, grad] = cost_function(test_theta, X, y)

print(f'Cost at test theta: {cost}\n'
      f'Expected cost (approx): 0.218\n'
      f'Gradient at test theta (zeros):\n'
      f'{grad}\n'
      f'Expected gradients (approx):\n 0.043\n 2.566\n 2.647\n')

# Part 3: Optimizing using fmin
theta = fmin(lambda x: cost_function(x, X, y)[0], initial_theta, disp=False)
[cost, grad] = cost_function(theta, X, y)

# Print theta to screen
print(f'Cost at theta found by fminunc: {cost}')
print(f'Expected cost (approx): 0.203')
print(f'theta:')
print(f' {theta}')
print(f'Expected theta (approx):')
print(f' -25.161\n 0.206\n 0.201')

# Plot boundary
fig = plt.figure()
ax = fig.add_subplot(111)
plot_decision_boundary(ax, theta, X, y, labels=['Admitted', 'Not admitted'])
ax.set_xlabel('Exam 1 score')
ax.set_ylabel('Exam 2 score')

# Part 4: Predict and Accuracies
prob = sigmoid(np.dot([1, 45, 85], theta))
print(f'For a student with scores 45 and 85, we predict an admission'
      f' probability of {prob}')
print(f'Expected value: 0.775 +/- 0.002\n')

# Compute accuracy on outr training set
p = predict(theta, X)
print(f'Train accuracy: {np.mean(p == y) * 100}%')
print(f'Expected accuracy = 89.0%')
plt.show()