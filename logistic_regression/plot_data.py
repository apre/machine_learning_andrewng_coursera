import numpy as np
from map_feature import map_feature


def plot_data(ax, X, y, labels=None):
    """

    :param ax: Matplotlib axis object
    :param X: dataset point vector. Shape is (num_examples, num_features)
    :param y: dataset value vector. Shape is (num_examples,)
    :param labels: plot label tuple: (positive_label, negative_label)
    """
    positive = X[y == 1]
    negative = X[y == 0]

    if labels is None:
        ax.scatter(*positive.T, marker='+', c='r')
        ax.scatter(*negative.T, marker='o', c='b')
    else:
        ax.scatter(*positive.T, marker='+', c='r', label=labels[0])
        ax.scatter(*negative.T, marker='o', c='b', label=labels[1])


def plot_decision_boundary(ax, theta, X, y, labels=None):
    """
    Plots the data points X and y into a new figure with the decision
    boundary defined by theta

    :param ax: Matplotlib axis object
    :param theta: regression parameters
    :param X: is assumed to be a (num_examples, num_features) np.array,
        where the first column (:, 0) is all-ones
    :param y: dataset value vector. Shape is (num_examples,)
    """
    plot_data(ax, X[:, 1:3], y, labels)  # Take out all-ones column from X

    if X.shape[1] == 3:
        # Only need 2 points to define a line, so choose 2 endpoints
        plot_x = np.array([np.min(X[:, 1]) - 2,
                           np.max(X[:, 1]) + 1])

        # Calculate the decision boundary line
        plot_y = -1/theta[2] * (theta[1] * plot_x + theta[0])

        # Plot
        ax.plot(plot_x, plot_y)
    else:
        # Here is the grid range
        u = np.linspace(-1, 1.5, 50)
        v = np.linspace(-1, 1.5, 50)
        z = np.zeros([len(u), len(v)])

        points = map_feature(u, v)
        # Evaluate z = theta * x over the grid
        for i in range(len(u)):
            for j in range(len(v)):
                z[i, j] = np.dot(map_feature(u[i: i+1], v[j: j+1]), theta)
        z = z.T

        ax.contour(u, v, z)
