import numpy as np
from sigmoid import sigmoid


def cost_function(theta, X, y):
    """
    Compute cost and gradient for logistic regression

    :param theta: parameter vector. Shape is (num_features,)
    :param X: dataset point vector. Shape is (num_examples, num_features)
    :param y: dataset value vector. Shape us (num_examples,)
    :return (J, grad): cost and gradient values w.r.t. parameters.
    """
    num_examples = len(y)
    positive_parcel = np.dot(y, np.log(sigmoid(np.dot(X, theta))))
    negative_parcel = np.dot(1-y, np.log(1 - sigmoid(np.dot(X, theta))))
    J = -(positive_parcel + negative_parcel)/num_examples

    error = sigmoid(np.dot(X, theta)) - y  # this is (num_examples, )
    grad = np.dot(error, X)/num_examples  # this is (num_features, )

    return J, grad


def cost_function_reg(theta, X, y, lamb):
    """
    Compute cost and gradient for logistic regression with regularization

    :param theta: parameter vector. Shape is (num_features,)
    :param X: dataset point vector. Shape is (num_examples, num_features)
    :param y: dataset value vector. Shape is (num_examples,)
    :param lamb: regularization parameter
    :return:
    """
    # Compute cost without regularization
    num_examples = len(y)
    J, grad = cost_function(theta, X, y)

    # Adding regularization cost
    J = J + lamb/2 * np.mean(np.square(theta))
    grad = grad + lamb/num_examples * theta

    return J, grad
